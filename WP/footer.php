<?php wp_footer(); ?>

  <!-- jQuery first, then Bootstrap JS. -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
  <script>
    function initMap() {
      var myLatLng = {
        lat: 45.47115279999999,
        lng: 4.395241400000032
      };
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: {
          lat: 45.47115279999999,
          lng: 4.395241400000032
        },
        disableDefaultUI: true,
        scrollwheel: false
      });
      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Centre d affaires Les Bureaux de Montreynaud'
      });
    }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdwChifdCCMB_lyo2DMrMgWYak9L0sWrU
&callback=initMap">
  </script>
  <script>
    $(function() {
      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });
  </script>
</body>

</html>
