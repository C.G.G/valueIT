<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>
<body>
  <div id="top"></div>
  <nav class="navbar navbar-fixed-top navbar-light bg-faded">
    <img class="mobile-logo hidden-md-up" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/logo.svg">
    <button class="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#nav">
      &#9776;
    </button>
    <div class="navbar-toggleable-sm collapse" id="nav">
      <a class="navbar-brand hidden-sm-down" href="#top"><img src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/logo.svg"></a>
      <ul class="nav navbar-nav pull-sm-right">
        <li class="nav-item">
          <a class="nav-link text-xs-center" href="#about">ABOUT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-xs-center" href="#certifications">CERTIFICATIONS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-xs-center" href="#solutions">SOLUTIONS</a>
        </li>
        <li class="nav-item contact-button">
          <a class="nav-link text-xs-center" href="#contact">CONTACT</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container-fluid">
    <div class="row" id="intro">
      <img id="intro-overlay" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/img-header.png">
      <img class="intro-icon intro-server hidden-xs-down" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-server.svg">
      <img class="intro-icon intro-security hidden-xs-down" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-security.svg">
      <img class="intro-icon intro-telecom hidden-xs-down" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-telecom.svg">
      <img class="intro-icon intro-services hidden-xs-down" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-services.svg">
      <div class="col-xs-12 intro-content">
        <h1 class="text-xs-center">Nous apprécions vos solutions informatiques</h1>
        <h2 class="text-xs-center">La confiance de certaines des entreprises les plus prestigieuses dans le monde</h2>
        <a href="infrastructure.html">
          <button type="button" class="btn center-block">Solutions</button>
        </a>
      </div>
    </div>
    <div class="row" id="about">
      <div class="col-xs-12">
        <h1 class="text-xs-center">Qui sommes-nous?</h1>
        <p>Le système d’information est au cœur de chaque entreprise, élément névralgique du bon fonctionnement de votre activité, il est primordial de trouver le partenaire qui saura vous accompagner et vous conseiller.
          <br>
          <br><strong>VALUE IT est le partenaire qu’il vous faut.</strong>
          <br>
          <br>Créée par 3 associées VALUE IT se veut être une SSII à taille humaine, réactive et disponible pour ses clients. Avec deux agences (Saint Etienne et Paris) nous avons la possibilité de répondre à tous vos projets en infrastructure informatique
          et télécom.
          <br>
          <br>VALUE IT n’est pas un simple intégrateur systèmeafin et réseaux, nous vous proposons une large gamme de services qui vous permettront d’avoir un interlocuteur unique de répondre aux différentes problématiques métiers que vous rencontrez.</p>
      </div>
      <div class="col-sm-4">
        <img class="center-block" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/img-philippebouaziz.png">
        <h2 class="text-xs-center">Philippe Bouaziz</h2>
        <p class="dim justify">Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus
          mollis interdum. Donec ullamcorper nulla non metus auctor fringilla.</p>
      </div>
      <div class="col-sm-4">
        <img class="center-block" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/img-marcsilbermann.png">
        <h2 class="text-xs-center">Marc Silbermann</h2>
        <p class="dim justify">Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus
          mollis interdum. Donec ullamcorper nulla non metus auctor fringilla.</p>
      </div>
      <div class="col-sm-4">
        <img class="center-block" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/img-jeremyorlowski.png">
        <h2 class="text-xs-center">Jeremy Orlowski</h2>
        <p class="dim justify">Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean lacinia bibendum nulla sed consectetur. Maecenas faucibus
          mollis interdum. Donec ullamcorper nulla non metus auctor fringilla.</p>
      </div>
      <div class="col-xs-12"></div>
    </div>
    <div class="row" id="certifications">
      <div class="col-xs-12">
        <h1 class="text-xs-center">Certifications</h1>
        <p class="text-xs-center">Nous avons été précieux dans certaines entreprises très prestigieuses.</p>
        <div class="row">
          <div class="col-md-3">
            <a href="http://www.hp.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-hp.svg"></a>
            <p class="text-xs-center">www.hp.com</p>
          </div>
          <div class="col-md-3">
            <a href="http://www.dell.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-dellgrey.svg"></a>
            <p class="text-xs-center dim">www.dell.com</p>
          </div>
          <div class="col-md-3">
            <a href="http://www.microsoft.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-microsoftgrey.svg"></a>
            <p class="text-xs-center dim">www.microsoft.com</p>
          </div>
          <div class="col-md-3">
            <a href="http://www.kaspersky.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-kasperskygrey.svg"></a>
            <p class="text-xs-center dim">www.kaspersky.com</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <a href="http://www.veeam.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-veeamgrey.svg"></a>
            <p class="text-xs-center dim">www.veeam.com</p>
          </div>
          <div class="col-md-3">
            <a href="http://www.datacore.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-datacoregrey.svg"></a>
            <p class="text-xs-center dim">www.datacore.com</p>
          </div>
          <div class="col-md-3">
            <a href="http://www.citrix.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-citrixgrey.svg"></a>
            <p class="text-xs-center dim">www.citrix.com</p>
          </div>
          <div class="col-md-3">
            <a href="http://www.symantec.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-symantecgrey.svg"></a>
            <p class="text-xs-center dim">www.symantec.com</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <a href="http://www.watchguard.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-watchguardgrey.svg"></a>
            <p class="text-xs-center dim">www.watchguard.com</p>
          </div>
          <div class="col-md-3">
            <a href="http://www.cisco.com"><img class="center-block grayscale" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-ciscogrey.svg"></a>
            <p class="text-xs-center dim">www.cisco.com</p>
          </div>
          <div class="col-md-3">
            <a href="http://www.vmware.com"><img class="center-block grayscale vmware" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-vmwaregrey.svg"></a>
            <p class="text-xs-center dim">www.vmware.com</p>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    </div>
    <div class="row" id="solutions">
      <div class="col-xs-12">
        <h1 class="text-xs-center">Solutions</h1>
        <div class="row top-solutions">
          <div class="col-md-4 first-solution">
            <img class="center-block" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-server.svg">
            <h3 class="text-xs-center">VALUEIT</h3>
            <h2 class="text-xs-center">Infrastructure Serveur</h2>
            <p class="text-xs-center first-p">Virtualisation - Accés distants / Mobilité - PRA/PCA - Collaboration - Messagerie Collaborative - Téléphonie IP.</p>
            <div class="row">
              <a href="infrastructure.html">
                <button type="button" class="btn center-block">plus d'informations</button>
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <img class="center-block" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-security.svg">
            <h3 class="text-xs-center">VALUEIT</h3>
            <h2 class="text-xs-center">Sécurité</h2>
            <br class="h2br">
            <p class="text-xs-center">Firewall - Antispam - Hébergement - Cloud Privé - Cloud Public.</p>
            <div class="row solution-2">
              <a href="securite.html">
                <button type="button" class="btn center-block">plus d'informations</button>
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <img class="center-block" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-services.svg">
            <h3 class="text-xs-center">VALUEIT</h3>
            <h2 class="text-xs-center">Services</h2>
            <br class="h2br">
            <p class="text-xs-center">Installation - Infogérance - Formation.</p>
            <div class="row solution-3">
              <a href="services.html">
                <button type="button" class="btn center-block">plus d'informations</button>
              </a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <img class="center-block" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-telecom.svg">
            <h3 class="text-xs-center">VALUEIT</h3>
            <h2 class="text-xs-center">Telecom</h2>
            <p class="text-xs-center bottom-row-p">Accés Internet Haut Débit - Accés TOIP/ Numéris/analogique.</p>
            <a href="telecom.html">
              <button type="button" class="btn center-block">plus d'informations</button>
            </a>
          </div>
          <div class="col-md-4 email-solution">
            <img class="center-block solutions-last" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-services.svg">
            <h2 class="text-xs-center solutions-last">null</h2>
            <p class="text-xs-center dim bottom-row-p">S'il y a d'autres solutions que vous cherchez s'il vous plaît entrer en contact et nous pourrons peut-être vous aider.</p>
            <a href="#contact">
              <button type="button" class="btn center-block contact-btn">Contact</button>
            </a>
          </div>
          <div class="col-md-4 nk-solution"></div>
        </div>
      </div>
    </div>
    <div class="row" id="contact">
      <div class="col-xs-12">
        <h1 class="text-xs-center">Contactez nous</h1>
      </div>
      <div class="contact-form col-xs-12 col-sm-6">
        <form>
          <fieldset class="form-group">
            <label for="InputName">Que devrions-nous vous appeler?</label>
            <input type="text" class="form-control" id="InputName" placeholder="John Doe">
          </fieldset>
          <fieldset class="form-group">
            <label for="InputEmail">Quelle adresse email doit-on répondre?</label>
            <input type="email" class="form-control" id="InputEmail" placeholder="exemple@exemple.com">
          </fieldset>
          <fieldset class="form-group">
            <label for="InputTextarea">Comment pouvons-nous vous aider?</label>
            <textarea class="form-control" id="InputTextarea" rows="3" placeholder="Tapez votre messager ici..."></textarea>
          </fieldset>
          <button type="submit" class="btn btn-primary submit">Soumettre</button>
        </form>
      </div>
      <div class="contact-info col-xs-12 col-sm-6">
        <div class="row">
          <div class="col-xs-1 hidden-xs-down">
            <img class="first" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-home.svg">
          </div>
          <div class="col-xs-12 col-sm-11">
            <p class="center-block first-p-mobile"><span class="first-mobile"><img class="first" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-home.svg"></span>Centre d'affaires Les Bureaux de Montreynaud 2, allée G, Puccini 42000 Saint-Etienne</p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-1 hidden-xs-down">
            <img src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-phone.svg">
          </div>
          <div class="col-xs-12 col-sm-11">
            <p><span><img src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-phone.svg"></span>04 77 00 00 00</p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-1 hidden-xs-down">
            <img src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-email.svg">
          </div>
          <div class="col-xs-12 col-sm-11">
            <a href="mailto:contact@value-info.fr">
              <p><span><img src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-email.svg"></span>contact@value-info.fr</p>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row" id="location">
      <div class="col-xs-12">
        <div id="map"></div>
        <a href="https://www.google.com/maps?ll=45.471153,4.395241&z=17&t=m&hl=en-US&gl=US&mapclient=apiv3">
          <div id="mobile-map"></div>
        </a>
      </div>
    </div>
    <div class="row" id="footer">
      <div class="col-xs-12 text-xs-center">
        <p>Copyright © 2016, ValueIT</p>
      </div>
    </div>
  </div>
<?php get_footer(); ?>
