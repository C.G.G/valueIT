<?php
/*
Template Name: Infrastructure Page
*/
?>
<?php get_header(); ?>
<body>
  <nav class="navbar navbar-fixed-top navbar-light bg-faded">
    <img class="solutions-site-logo center-block" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/logo.svg">

    <div class="navbar" id="nav">
      <ul class="nav navbar-nav back-nav">
        <li class="nav-item backlink-item">
          <a class="nav-link text-xs-center backlink" href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/"><img class="backicon" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-back.svg"></a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container-fluid">
    <div class="row" id="solutions-intro">
      <img id="intro-overlay" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/img-header.png">
      <img id="solutions-intro-icon" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/icon-server.svg">
      <div class="col-xs-12 intro-content">
        <h2 class="text-xs-center">SOLUTIONS</h2>
        <h1 class="text-xs-center">Infrastructure Serveur</h1>
      </div>
    </div>
    <div class="row" id="solutions-page">
      <div class="solutions-page-nav col-xs-12">
        <a href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/infrastructure">
          <button type="button" class="btn solutions-page-link solutions-current" href="#">Infrastructure
            <br>Serveur</button>
        </a>
        <a href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/securite">
          <button type="button" class="btn solutions-page-link">Sécurité</button>
        </a>
        <a href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/services">
          <button type="button" class="btn solutions-page-link">Services</button>
        </a>
        <a href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/telecom">
          <button type="button" class="btn solutions-page-link">Telecom</button>
        </a>
      </div>
      <div class="col-xs-12">
        <h1>Virtualisation</h1>
        <hr><span class="solutions-bar"></span>
        <p>La virtualisation est aujourd’hui une solution incontournable. Que ce soit la virtualisation des serveurs, Postes de Travail ou Application. Value IT saura vous proposer des solutions innovantes autour des leaders de la virtualisation VmWare,
          Microsoft et Citrix.</p>
        <h1>Accés distants / Mobilité</h1>
        <hr><span class="solutions-bar"></span>
        <p>La mobilité de vos collaborateurs et le télétravail sont aujourd’hui de vrais éléments différenciant qui vous permettront d’améliorer votre productivité mais aussi le bien être de vos salariés. VALUE IT vous accompagnera dans la mise en place
          de fermes Citrix ou Microsoft RDS afin de mettre en place un système disponible en tous lieux de manière sécurisée.</p>
        <h1>PRA/PCA</h1>
        <hr><span class="solutions-bar"></span>
        <p>L’architecture informatique de votre entreprise doit fonctionner sans interruption, la moindre coupure entraine une perte de chiffre d’affaire, stop la production et désorganise votre société. En tant qu’expert en Infrastructure informatique,
          VALUE IT saura vous présentez des solutions sur mesure répondant à vos besoins tant en terme d’Objectif de Temps de Rétablissement qu’en terme d’Objectif de période de restauration des données (RTO & RPO).</p>
        <h1>Collaboration</h1>
        <hr><span class="solutions-bar"></span>
        <p><strong>Messagerie Collaborative</strong> L’email est aujourd’hui le moyen de communication préféré des entreprises, impossible de travailler sans un système de messagerie professionnel. VALUE IT vous aidera à transformer un simple système de
          messagerie en solution de messagerie collaborative qui vous permettra d’améliorer considérablement le travail d’équipe grâce aux partages de calendriers, de contact, gestion de ressource partagée…</p>
        <h1>Téléphonie IP</h1>
        <hr><span class="solutions-bar"></span>
        <p>La Téléphonie d’entreprise fait aujourd’hui partie intégrante du système informatique d’une entreprise. VALUE IT vous conseillera et vous accompagnera dans le choix d’une solution de TOIP adaptée à vos besoins tant en terme de matériels, de fonctionnalités
          (VisioConférence, Présentielle, Chat instantané…) et d’abonnements Voix.</p>
      </div>
    </div>
    <div class="row solutions-contact" id="contact">
      <div class="col-xs-12">
        <h1 class="text-xs-center">Contactez nous</h1>
        <p class="text-xs-center hidden-xs-down">S'il y a d'autres solutions que vous cherchez s'il vous plaît entrer en contact et nous pourrons peut-être vous aider.</p>
      </div>
      <div class="contact-form col-xs-12 col-sm-8 col-sm-offset-2">
        <form>
          <fieldset class="form-group">
            <label for="InputName">Que devrions-nous vous appeler?</label>
            <input type="text" class="form-control" id="InputName" placeholder="John Doe">
          </fieldset>
          <fieldset class="form-group">
            <label for="InputEmail">Quelle adresse email doit-on répondre?</label>
            <input type="email" class="form-control" id="InputEmail" placeholder="exemple@exemple.com">
          </fieldset>
          <fieldset class="form-group">
            <label for="InputTextarea">Comment pouvons-nous vous aider?</label>
            <textarea class="form-control" id="InputTextarea" rows="3" placeholder="Tapez votre messager ici..."></textarea>
          </fieldset>
          <button type="submit" class="btn btn-primary submit">Soumettre</button>
        </form>
      </div>
    </div>
    <div class="row" id="footer">
      <div class="col-xs-12 text-xs-center">
        <p>Copyright © 2016, ValueIT</p>
      </div>
    </div>
  </div>

<?php get_footer('solutions'); ?>
