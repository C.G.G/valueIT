<?php
/*
Template Name: Telecom Page
*/
?>
<?php get_header(); ?>
<body>
  <nav class="navbar navbar-fixed-top navbar-light bg-faded">
    <img class="solutions-site-logo center-block" src="img/logo.svg">

    <div class="navbar" id="nav">
      <ul class="nav navbar-nav back-nav">
        <li class="nav-item backlink-item">
          <a class="nav-link text-xs-center backlink" href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/"><img class="backicon" src="img/icon-back.svg"></a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container-fluid">
    <div class="row" id="solutions-intro">
      <img id="intro-overlay" src="img/img-header.png">
      <img id="solutions-intro-icon" src="img/icon-telecom.svg">
      <div class="col-xs-12 intro-content">
        <h2 class="text-xs-center">SOLUTIONS</h2>
        <h1 class="text-xs-center">Telecom</h1>
      </div>
    </div>
    <div class="row" id="solutions-page">
      <div class="solutions-page-nav col-xs-12">
        <a href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/infrastructure">
          <button type="button" class="btn solutions-page-link" href="#">Infrastructure
            <br>Serveur</button>
        </a>
        <a href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/securite">
          <button type="button" class="btn solutions-page-link">Sécurité</button>
        </a>
        <a href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/services">
          <button type="button" class="btn solutions-page-link">Services</button>
        </a>
        <a href="https://689984.admin.dc2.gpaas.net/vhosts/www.value-info.fr/htdocs/telecom">
          <button type="button" class="btn solutions-page-link solutions-current">Telecom</button>
        </a>
      </div>
      <div class="col-xs-12">
        <h1>Accés Internet Haut Débit</h1>
        <hr><span class="solutions-bar"></span>
        <p>Le Très Haut Débit est partout et devient primordial pour une entreprise. VALUE IT en véritable Fournisseur d’Accés Internet de proximité, vous proposera des solutions sur mesure allant de la fourniture d’un lien ADSL ou Fibre Optique à la fourniture
          de solution d’interconnexion complexe MPLS avec gestion de la QOS.</p>
        <h1>Accés TOIP/ Numéris/analogique</h1>
        <hr><span class="solutions-bar"></span>
        <p>Vous souhaitez remettre à plat vos abonnements téléphoniques, les factures sont devenues incompréhensibles, une évolution empirique de votre entreprise à entrainer la multiplication des abonnements, VALUE IT vous accompagnera dans l’analyse technique
          et financière de vos abonnements Voix.</p>
      </div>
    </div>
    <div class="row solutions-contact" id="contact">
      <div class="col-xs-12">
        <h1 class="text-xs-center">Contactez nous</h1>
        <p class="text-xs-center hidden-xs-down">S'il y a d'autres solutions que vous cherchez s'il vous plaît entrer en contact et nous pourrons peut-être vous aider.</p>
      </div>
      <div class="contact-form col-xs-12 col-sm-8 col-sm-offset-2">
        <form>
          <fieldset class="form-group">
            <label for="InputName">Que devrions-nous vous appeler?</label>
            <input type="text" class="form-control" id="InputName" placeholder="John Doe">
          </fieldset>
          <fieldset class="form-group">
            <label for="InputEmail">Quelle adresse email doit-on répondre?</label>
            <input type="email" class="form-control" id="InputEmail" placeholder="exemple@exemple.com">
          </fieldset>
          <fieldset class="form-group">
            <label for="InputTextarea">Comment pouvons-nous vous aider?</label>
            <textarea class="form-control" id="InputTextarea" rows="3" placeholder="Tapez votre messager ici..."></textarea>
          </fieldset>
          <button type="submit" class="btn btn-primary submit">Soumettre</button>
        </form>
      </div>
    </div>
    <div class="row" id="footer">
      <div class="col-xs-12 text-xs-center">
        <p>Copyright © 2016, ValueIT</p>
      </div>
    </div>
  </div>
<?php get_footer('solutions'); ?>
